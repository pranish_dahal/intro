<?php

class Application_Form_Person extends Zend_Form {

    public function init() {
        $name = $this->createElement('text', 'first_name', array(
            'label' => 'First Name',
            'placeholder' => 'First Name',
            'class'=>'form-control ',
            
        ));
        $name->addFilter('StringTrim');
        $name->setRequired(true);
        $this->addElement($name);

        $middleName = $this->createElement('text', 'middle_name', array(
            'label' => 'Middle Name',
            'placeholder' => 'Middle Name',
            'class'=>'form-control'
        ));
        $middleName->addFilter('StringTrim');
        $this->addElement($middleName);

        $lastName = $this->createElement('text', 'last_name', array(
            'label' => 'Last Name',
            'required' => true,
            'placeholder' => 'Last Name',
            'class'=>'form-control'
        ));
        $this->addElement($lastName);

        $this->addElement('radio', 'gender', array(
            'label' => 'Gender',
            'multiOptions' => array(
                'male' => 'Male',
                'female' => 'Female',
            ),
             'required' => true
        ));

        $dob = $this->createElement('text', 'dob', array(
            'label' => 'Date of Birth',
            'required' => true,
            'placeholder' => 'YYYY-MM-DD',
            'id' => 'dp1',
            'class' => 'date_picker',
            'class'=>'form-control'
        ));


        $validator = new Zend_Validate_Regex(
                '/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/');
        $validator->setMessage('The value is not a valid date');
        $dob->addValidator($validator, true);
        $this->addElement($dob);


        $phone = $this->createElement('text', 'phone_no', array(
            'label' => 'Phone No',
            'class'=>'form-control',
            'placeholder'=>'Phone Number'
        ));
        $this->addElement($phone); 
        
        $nationality = $this->createElement('text', 'nationality', array(
            'label' => 'Nationality',
            'required' => true,
            'placeholder' => 'Nationality',
            'class'=>'form-control'
        ));
        $this->addElement($nationality);

        $email = $this->createElement('text', 'email', array(
            'label' => 'Email',
            'class'=>'form-control',
             'placeholder'=>'Email',
             'required' => true,
        ));
        $email->addFilter('StringTrim');
        $this->addElement($email); 


        $address = $this->createElement('textarea', 'address', array(
            'label' => 'Address',
            'required' => true,
            'rows' => 3,
            'cols' => 50,
            'class'=>'form-control',
             'placeholder'=>'Address'
        ));
        $address->addFilter('StringTrim');
        $this->addElement($address);

        $educationBackground = $this->createElement('textarea', 'education', array(
            'label' => 'Education',
            'required' => true,
            'rows' => 3,
            'cols' => 50,
            'class'=>'form-control',
             'placeholder'=>'Education Background'
        ));
        $address->addFilter('StringTrim');
        $this->addElement($educationBackground);
        
          $this->addElement('radio', 'contactWay', array(
            'label' => 'Contact Way',
            'multiOptions' => array(
                'mail' => 'Mail',
                'phone' => 'Phone'
            )
        ));
        

        $this->addElement('Hash', 'csrf', array(
            'ignore' => true
        ));

        $submit = $this->createElement('submit', 'submit', array(
            'label' => 'Save Person ',
            'class' => 'btn btn-large btn-primary'
        ));
        $this->addElement($submit);
        
    }

}

