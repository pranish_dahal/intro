<?php

class Application_Model_Person {
    protected $error_messages = array();

    protected $personId = null;

    protected $firstName = null;

    protected $middleName = null;

    protected $lastName = null;

    protected $dob = null;

    protected $phoneNo = null;

    protected $address = null;

    protected $email = null;

    protected $gender = null;

    protected $education=null;
    
    protected $nationality=null;
    
    protected $contactWay=null;
    
   
    public function getErrorMessages()
    {
        return $this->error_messages;
    }

    public function setErrorMessages($messages)
    {
        if (is_array($messages)) {
                    $this->error_messages = $messages;
                } elseif (is_string($messages)) {
                        $this->error_messages[] = $messages;
                    } else {
                        throw new Zend_Exception(
                        'Message should either be array or string.');
                    }
    }

    public function __construct(array $options = null)
    {
        if (is_array($options)) {
                    $this->setOptions($options);
                }
    }

    public function __get($name)
    {
        $parts = explode('_', $name);
                foreach ($parts as $k => $v) {
                    $parts[$k] = ucfirst($v);
                }
                $method = 'get' . ucfirst(implode('', $parts));
                if (('mapper' == $name) || ! method_exists($this, $method)) {
                    throw new Exception('Invalid content property');
                }
                return $this->$method();
    }

    public function __set($name, $value)
    {
        $parts = explode('_', $name);
                foreach ($parts as $k => $v) {
                    $parts[$k] = ucfirst($v);
                }
                $method = 'set' . ucfirst(implode('', $parts));
                if (('mapper' == $name) || ! method_exists($this, $method)) {
                    throw new Exception('Invalid content property');
                }
                $this->$method($value);
    }

    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
                foreach ($options as $key => $value) {
                $parts=explode('_',$key);
                	foreach($parts as $k=>$v){
                		$parts[$k]=ucfirst($v);
                	}
                    $method = 'set' . ucfirst(implode('',$parts));
                    if (in_array($method, $methods)) {
                        $this->$method($value);
                    }
                }
                return $this;
    }
    
    public function save(){
        return Application_Model_PersonMapper::getInstance()->save($this);
    }

    public function getPersonId() {
        return $this->personId;
    }

    public function setPersonId($personId) {
        $this->personId = $personId;
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function getMiddleName() {
        return $this->middleName;
    }

    public function setMiddleName($middleName) {
        $this->middleName = $middleName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function getDob() {
        return $this->dob;
    }

    public function setDob($dob) {
        $this->dob = $dob;
    }

    public function getPhoneNo() {
        return $this->phoneNo;
    }

    public function setPhoneNo($phoneNo) {
        $this->phoneNo = $phoneNo;
    }

    public function getAddress() {
        return $this->address;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getGender() {
        return $this->gender;
    }

    public function setGender($gender) {
        $this->gender = $gender;
    }

    public function getEducation() {
        return $this->education;
    }

    public function setEducation($education) {
        $this->education = $education;
    }
    
     public function getNationality() {
        return $this->nationality;
    }

    public function setNationality($nationality) {
        $this->nationality = $nationality;
    }

    public function getContactWay() {
        return $this->contactWay;
    }

    public function setContactWay($contactWay) {
        $this->contactWay = $contactWay;
    }

        
    public function toArray()
    {
        $data = get_class_vars(__CLASS__);
                foreach ($data as $key => $value) {
                    $data[$key] = $this->$key;
                }
                return $data;
    }


}

?>
