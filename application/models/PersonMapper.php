<?php

class Application_Model_PersonMapper {

    protected static $_instance = null;
    private static $PERSON_DETAILS_CSV = "/../public/csv/person/person.csv";
    private static $APPEND = 'a';
    private static $WRITE = 'w';
    private static $READ = 'r';

    public static function getInstance() {
        if (!self::$_instance instanceof self) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function save(Application_Model_Person $person) {
        $personMeta = array(
            'firstName' => $person->getFirstName(),
            'middleName' => $person->getMiddleName(),
            'lastName' => $person->getLastName(),
            'gender ' => $person->getGender(),
            'phoneNo' => $person->getPhoneNo(),
            'email' => $person->getEmail(),
            'address' => $person->getAddress(),
            'nationality' => $person->getNationality(),
            'dob' => $person->getDob(),
            'education' => $person->getEducation(),
            'contactWay' => $person->getContactWay()
        );

        $this->insert($personMeta);
    }

   

    public function read() {
        $personList = array();
        $file = fopen($this->getPersonFilePath(), $this->performAction(Action::getRead()));
        while (!feof($file)) {
            $personList[] = fgetcsv($file);
        }
        fclose($file);
        return $personList;
    }

    private function doFileExist() {
        if (file_exists($this->getPersonFilePath())) {
            return true;
        } else {
            return false;
        }
    }

    private function performAction($toPerform) {
        if ($this->doFileExist()) {
            if ($toPerform == Action::getAppend()) {
                return Action::getAppend();
            } elseif ($toPerform == Action::getRead()) {
                return Action::getRead();
            }
        } else {
            return Action::getWrite();
        }
    }

    private function getPersonFilePath() {
        $targetFile = APPLICATION_PATH . self::$PERSON_DETAILS_CSV;
        return $targetFile;
    }
    
     private function convertToPerson(array $person) {
        $newPerson = new Application_Model_Person();
        $newPerson->setFirstName($this->getDataFrom($person, 'firstName'));
        $newPerson->setMiddleName($this->getDataFrom($person, 'middleName'));
        $newPerson->setLastName($this->getDataFrom($person, 'lastName'));
        $newPerson->setGender($this->getDataFrom($person, 'gender'));
        $newPerson->setPhoneNo($this->getDataFrom($person, 'phone'));
        $newPerson->setEmail($this->getDataFrom($person, 'email'));
        $newPerson->setAddress($this->getDataFrom($person, 'address'));
        $newPerson->setNationality($this->getDataFrom($person, 'nationality'));
        $newPerson->setDob($this->getDataFrom($person, 'dob'));
        $newPerson->setEducation($this->getDataFrom($person, 'education'));
        $newPerson->setContactWay($this->getDataFrom($person, 'contactWay'));
        return $newPerson;
    }

    private function getDataFrom(array $data, $value) {
        if ($value == 'firstName') {
            return $data[0];
        } elseif ($value == 'middleName') {
            return $data[1];
        } elseif ($value == 'lastName') {
            return $data[2];
        } elseif ($value == 'gender') {
            return $data[3];
        } elseif ($value == 'phone') {
            return $data[4];
        } elseif ($value == 'email') {
            return $data[5];
        } elseif ($value == 'address') {
            return $data[6];
        } elseif ($value == 'nationality') {
            return $data[7];
        } elseif ($value == 'dob') {
            return $data[8];
        } elseif ($value == 'education') {
            return $data[9];
        } elseif ($value == 'contactWay') {
            return $data[10];
        }
    }

    public function retrieve($cursor = 1, $limit = 10) {
        $length = $cursor * $limit;
        $start=$length-$limit;
        $allPerson = $this->read();
        $collection = array();
        if($length>count($allPerson)){
            $length=count($allPerson);
        }
        for ($i = $start; $i < $length; $i++) {
            if (is_array($allPerson[$i])) {
                $collection[] = $this->convertToPerson($allPerson[$i]);
            }
        }
        return $collection;
    }

    private function insert(array $person) {
        $targetFile = $this->getPersonFilePath();
        $perform = $this->performAction(Action::getAppend());
        $fp = fopen($targetFile, $perform);
        fputcsv($fp, $person);
        fclose($fp);
    }

}

class Action {

    protected static $_instance = null;
    private static $read = 'r';
    private static $write = 'w';
    private static $append = 'a';

    public static function getInstance() {
        if (!self::$_instance instanceof self) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public static function getRead() {
        return self::$read;
    }

    public static function getWrite() {
        return self::$write;
    }

    public static function getAppend() {
        return self::$append;
    }

}

?>
