<?php

class PersonController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }
    
    public function indexAction()
    {
        $limitLength=$this->getLimitLength();
        $this->view->limit=$limitLength;
         $persons=array();
        if($limitLength){
            $persons=  Application_Model_PersonMapper::getInstance()->retrieve();
        }
        $this->view->data=$persons;
    }
    
    private function getLimitLength(){
         $singleLimit=10;
        $personList=  Application_Model_PersonMapper::getInstance()->read();
        $personCount=  count($personList)-1;
        return ceil($personCount/$singleLimit);
    }

    public function getAction()
    {
       
        $limitLength=$this->getLimitLength();
        $this->view->limit=$limitLength;
        
        $cursor=$this->getRequest()->getParam('cursor',1);
        $person=  Application_Model_PersonMapper::getInstance();
        $personList=$person->retrieve($cursor);
        $this->view->data=$personList;
    }
    
}

