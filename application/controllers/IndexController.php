<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $form=new Application_Form_Person();
        $this->view->form=$form;
        
        if($this->getRequest()->getPost())
        {
            if($form->isValid($this->getRequest()->getPost()))
                {
                    $person=new Application_Model_Person($form->getValues());
                    $person->save();
                }
        }
    }
    
    public function listAction(){
        $cursor=$this->getRequest()->getParam('cursor',1);
        $person=  Application_Model_PersonMapper::getInstance();
        $personList=$person->retrieve($cursor);
        print_r($personList);
    }


}

